/*************************************
Hyper-effective labyrint solver
(c) jadus - 2014
*************************************/

#include <stdio.h>
#include <stdlib.h>

enum Tile {
	wall,
	space,
	coin,
	player,
	visited,
	path
};

int print(enum Tile ** bludiste, int w, int h);
int go(enum Tile ** bludiste, int x, int y);


int print(enum Tile ** bludiste, int w, int h) {
	int i,j;
	for(i=0; i<h; i++) {
		for(j=0; j<w; j++)
			switch(bludiste[i+1][j+1]) {
				case wall:
					printf("█");
				break;

				case space:
					printf(" ");
				break;
				
				case visited:
					printf(" ");
				break;


				case coin: 
					printf("€");
				break;

				case player:
					printf("☻");
				break;

				case path:
					printf("░");
				break;
			
			}
		printf("\n");
	}

	return 0;
}

int go(enum Tile ** bludiste, int x, int y) {
	
	system("clear");
	bludiste[x][y] = player;
	print(bludiste, 30, 20);
	
	if(bludiste[x+1][y] == coin ||
	   bludiste[x-1][y] == coin ||
	   bludiste[x][y+1] == coin ||
	   bludiste[x][y-1] == coin)
		return 1;

	getc(stdin);
	bludiste[x][y] = visited;
	
	if(bludiste[x][y+1] == space) {
		if(go(bludiste, x, y+1)) {
			bludiste[x][y+1] = path;
			return 1;
		}
				
	}
	
	if(bludiste[x][y-1] == space) {
		if(go(bludiste, x, y-1)) {
			 bludiste[x][y-1] = path;
			 return 1;
		}
	
	}
	
	if(bludiste[x+1][y] == space) {
		if(go(bludiste, x+1, y)) { 
			bludiste[x+1][y] = path;
			return 1;
		}
	}
	
	if(bludiste[x-1][y] == space) {
		if(go(bludiste, x-1, y)) {
			bludiste[x-1][y] = path;	
			return 1;
		}
	}
	

	return 0;
		
}

int main() {
	enum Tile ** bludiste; // Protoze staticky pole jsou nuda!
	int i,j,k;
	int width, height;
	int startx, starty; // Ano, vim, ze ty radky a sloupce jsou prohozene, ale takhle se mi v tom lip orientuje
	FILE *fiel;	
	char *line;	
	
	fiel = fopen("bludiste.blu", "r");
	fscanf(fiel, "%d", &height);
	fscanf(fiel, "%d", &width);
	fscanf(fiel, "%d", &startx);
	fscanf(fiel, "%d", &starty);
	line = (char *)malloc(width);
	bludiste = (enum Tile **)malloc(sizeof(enum Tile *) * (height+2));

	for(i=0; i< height+2; i++)
		*(bludiste+i) = (enum Tile *)malloc(sizeof(enum Tile) * (width+2));


	for(i=0; i < height+2; i++) {
		for(j=0; j < width+2; j++)
			bludiste[i][j] = wall;
			
	}
	
	for(i=0; i<height; i++) {
		fscanf(fiel, "%s\n", line);
		for(j=0; j<width; j++) {			
			switch(line[j]) {
				case 'X':
					bludiste[i+1][j+1] = wall;
				break;

				case '0':
					bludiste[i+1][j+1] = space;
				break;

				case 'P':
					bludiste[i+1][j+1] = coin;
				break;


			}

		}
	}
	
	bludiste[startx][starty] = player;
	if(go(bludiste, startx, starty))
		printf("Here we go!\n");	
	print(bludiste, width, height);
	return 0;
	
}
